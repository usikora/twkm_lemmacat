# TWKM: LemmaCat
> Lemma Katalog XML für das TWKM Projekt zum Erfassen von Lemmata


## Download
- **GitLab**: 
  - clone this Repository from GitLab using the "Clone" Button 
  - ... OR just **download** it using the button right next to the "Clone" Button 
- **Git**: 
  - Using HTTPS: `git clone https://gitlab.gwdg.de/entities/entityxml.git`
  - Using SSH: `git clone git@gitlab.gwdg.de:entities/entityxml.git`

Then move it to a directory on your filesystem where you like to have it.


## How to use?
If you want to keep it simple without downloading the framework copy these Processing Instructions into an empty XML-File and go:

```xml
<?xml-model href="https://gitlab.gwdg.de/usikora/twkm_lemmacat/-/raw/master/schema/lemmacat.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.gwdg.de/usikora/twkm_lemmacat/-/raw/master/schema/lemmacat.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/usikora/twkm_lemmacat/-/raw/master/assets/css/lemmacat.author.css" title="lemmacat" alternate="no"?>
```
These Instructions reference the schema, its schematron validation routines and the author mode css right from the GitLab. This (and an online connection) is everything to start working with TWKM: LemmaCat in oXygen. If you prefere more, just download the framework as described below and integrate it into oXygen.

### entityXML oXygen Framework
TWKM: LemmaCat comes with a Document Type Association Framework that can be used by oXygen XML Editor. If you integrate it you can use Conversion and more interactive features in the Author Mode!

#### Loose Integration as Additonal Framework 
- Go to `Optionen` > `Einstellungen`
- Choose `Dokumenttypen-Zuordnung` from the List on the left and chose the subentry `Orte`
  - Add the path to the parent directory of the downloaded LemmaCat directory (see above) to `Zusätzliche Framework-Verzeichnisse`
  - Have a look at the List of oxygen-frameworks by clicking on `Dokumenttypen-Zuordnung`. If you find a `TWKM: LemmaCat` entry you were succesfull and can begin working.


  
