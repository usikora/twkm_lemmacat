<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:rng="http://relaxng.org/ns/structure/1.0"
    xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization"
    xmlns:xpath="http://www.w3.org/2005/xpath-functions"
    exclude-result-prefixes="xs" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/models/lemmacat"
    version="3.0">
    
    
    <!--<xsl:output method="xml" indent="true"/>
    <xsl:template match="/">
        <xsl:variable name="json-xml">
            <xsl:apply-templates mode="json-xml" />
        </xsl:variable>
        <xsl:copy-of select="$json-xml"></xsl:copy-of>
    </xsl:template>-->
    
    <xsl:output method="text"/>
    <xsl:template match="/">
        <xsl:variable name="json-xml">
            <xsl:apply-templates mode="json-xml" />
        </xsl:variable>
        <xsl:value-of select="xml-to-json($json-xml)"/>
    </xsl:template>
    
    <xsl:template match="lemmacat" mode="json-xml">
        <xpath:map>
            <xpath:array key="lemmacat">
                <xsl:apply-templates mode="json-xml" select="lemma"/>
            </xpath:array>
            <xpath:array key="sources">
                <xsl:apply-templates mode="json-xml" select="sources"/>
            </xpath:array>
        </xpath:map>
    </xsl:template>
    
    <xsl:template match="bibl" mode="json-xml">
        <xpath:map>
            <xsl:apply-templates mode="json-xml" select="@*"/>
            <xpath:string key="text">
                <xsl:value-of select="."/>
            </xpath:string>
        </xpath:map>
    </xsl:template>
    
    <xsl:template match="lemma|entry" mode="json-xml">
        <xpath:map>
            <xsl:apply-templates mode="json-xml" select="@*"/>
            <xsl:call-template name="collect-forms"/>
            <!--<xsl:for-each-group select="form" group-by="@type">
                <xpath:map key="{@type}">
                    <xsl:apply-templates mode="json-xml" select="@*[not(name()='type')]"/>
                    <xpath:array key="items">
                        <xsl:apply-templates mode="json-xml" select="element()"/>
                    </xpath:array>
                </xpath:map>
            </xsl:for-each-group>-->
            <xsl:apply-templates mode="json-xml" select="gramGrp"/>
            <xsl:call-template name="collect-defs"/>
            <xsl:for-each select="seeAlso">
                <xpath:array key="{@type}">
                    <xsl:apply-templates mode="json-xml" select="."/>
                </xpath:array>
            </xsl:for-each>
            <xsl:call-template name="features"/>
            <xsl:call-template name="notes"/>
        </xpath:map>
    </xsl:template>
    
    <xsl:template name="collect-forms">
        <xpath:array key="forms">
            <xsl:for-each select="./form">
                <xsl:apply-templates mode="json-xml" select="self::form"/>
            </xsl:for-each>
        </xpath:array>
    </xsl:template>
    
    <xsl:template match="form" mode="json-xml">
        <xpath:map>
            <xsl:apply-templates mode="json-xml" select="@*"/>
            <xpath:array key="terms">
                <xsl:apply-templates mode="json-xml" select="./element()"/>
            </xpath:array>
        </xpath:map>
    </xsl:template>
    
    <!--<xsl:template match="reading|spelling" mode="json-xml">
        <xpath:array key="{name()}s">
            <xsl:apply-templates mode="json-xml" select="@*"/>
            <xsl:apply-templates mode="json-xml" select="item|term"/>
        </xpath:array>
    </xsl:template>-->
    
    <xsl:template match="gramGrp" mode="json-xml">
        <xpath:array key="gramGrp">
            <xsl:apply-templates mode="json-xml"/>
        </xpath:array>
    </xsl:template>
    
    <!--<xsl:template match="pos" mode="json-xml">
        <xpath:map>
            <xpath:string key="type">pos</xpath:string>
            <xpath:string key="value"><xsl:value-of select="normalize-space(text())"/></xpath:string>
        </xpath:map>
    </xsl:template>-->
    
    <xsl:template match="gram" mode="json-xml">
        <xpath:map>
            <xpath:string key="type"><xsl:value-of select="@type"/></xpath:string>
            <xpath:string key="value"><xsl:value-of select="normalize-space(text())"/></xpath:string>
        </xpath:map>
    </xsl:template>
    
    <xsl:template match="term" mode="json-xml">
        <xpath:map>
            <xsl:apply-templates mode="json-xml" select="@*"/>
            <xpath:string key="value">
                <xsl:value-of select="normalize-space(text())"/>
            </xpath:string>
        </xpath:map>
    </xsl:template>
    
    <!--<xsl:template match="translation" mode="json-xml">
        <xpath:string key="def">
            <xsl:value-of select="normalize-space(text())"/>
        </xpath:string>
    </xsl:template>-->
    
    <xsl:template match="def" mode="json-xml">
        <xpath:map>
            <xsl:apply-templates mode="json-xml" select="@*"/>
            <xpath:string key="value">
                <xsl:value-of select="normalize-space(text())"/>
            </xpath:string>
        </xpath:map>
    </xsl:template>
    
    <!--<xsl:template match="def" mode="json-xml">
        <xpath:array key="def">
            <xsl:choose>
                <xsl:when test="child::term">
                    <xsl:apply-templates mode="json-xml"/>
                </xsl:when>
                <xsl:otherwise>
                    <xpath:string>
                        <xsl:value-of select="normalize-space(text())"/>
                    </xpath:string>
                </xsl:otherwise>
            </xsl:choose>
        </xpath:array>
    </xsl:template>-->
    
    
    
    <!--<xsl:template match="alternatives|dictionaries" mode="json-xml">
        <xpath:array key="{name()}">
            <xsl:apply-templates mode="json-xml"/>
        </xpath:array>
    </xsl:template>-->
    
    <xsl:template match="seeAlso" mode="json-xml">
        <xsl:apply-templates mode="json-xml"/>
    </xsl:template>
    
    <xsl:template match="@*" mode="json-xml">
        <xpath:string key="@{name()}">
            <xsl:value-of select="normalize-space(./data())"/>
        </xpath:string>
    </xsl:template>
    
    <xsl:template name="features">
        <xsl:for-each-group select="feat" group-by="@type">
            <xpath:array key="{@type}">
                <!--<xsl:for-each select="xpath:current-group()"></xsl:for-each>-->
                <xsl:apply-templates mode="json-xml" select="current-group()"/>
            </xpath:array>
        </xsl:for-each-group>
    </xsl:template>
    
    <xsl:template match="feat" mode="json-xml">
        <xpath:map>
            <xsl:apply-templates mode="json-xml" select="@*"/>
            <xpath:array key="items">
                <xsl:apply-templates mode="json-xml" select="item|term"/>
            </xpath:array>
        </xpath:map>
    </xsl:template>
    
    <xsl:template name="notes">
        <xsl:for-each-group select="note" group-by="name()">
            <xpath:array key="notes">
                <!--<xsl:for-each select="xpath:current-group()"></xsl:for-each>-->
                <xsl:apply-templates mode="json-xml" select="current-group()"/>
            </xpath:array>
        </xsl:for-each-group>
    </xsl:template>
    
    <xsl:template match="note" mode="json-xml">
        <xpath:string>
            <xsl:value-of select="normalize-space(text())"/>
        </xpath:string>
    </xsl:template>
    
    <xsl:template name="collect-defs">
        <xpath:array key="definitions">
            <xsl:for-each select="./def">
                <xsl:apply-templates mode="json-xml" select="."/>
            </xsl:for-each>
        </xpath:array>
    </xsl:template>
    
    
    
</xsl:stylesheet>